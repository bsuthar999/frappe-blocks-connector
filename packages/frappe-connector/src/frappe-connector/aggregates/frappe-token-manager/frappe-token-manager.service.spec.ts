import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/common';
import { FrappeTokenManagerService } from './frappe-token-manager.service';
import { SettingsService } from '../../../system-settings/controllers/settings/settings.service';
import { RequestStateService } from '../../entities/request-state/request-state.service';
import { FrappeTokenService } from '../../entities/frappe-token/frappe-token.service';
import { FrappeClientService } from '../../entities/frappe-client/frappe-client.service';

describe('FrappeTokenManagerService', () => {
  let service: FrappeTokenManagerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        FrappeTokenManagerService,
        { provide: SettingsService, useValue: {} },
        { provide: RequestStateService, useValue: {} },
        { provide: FrappeTokenService, useValue: {} },
        { provide: FrappeClientService, useValue: {} },
      ],
    }).compile();

    service = module.get<FrappeTokenManagerService>(FrappeTokenManagerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
