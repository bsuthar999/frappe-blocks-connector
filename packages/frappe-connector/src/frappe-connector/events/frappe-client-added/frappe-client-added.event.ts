import { IEvent } from '@nestjs/cqrs';
import { FrappeClient } from '../../entities/frappe-client/frappe-client.entity';

export class FrappeClientAddedEvent implements IEvent {
  constructor(public readonly client: FrappeClient) {}
}
