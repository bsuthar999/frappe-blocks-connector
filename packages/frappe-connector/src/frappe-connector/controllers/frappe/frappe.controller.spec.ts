import { Test, TestingModule } from '@nestjs/testing';
import { FrappeController } from './frappe.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { FrappeTokenManagerService } from '../../aggregates/frappe-token-manager/frappe-token-manager.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { HttpService } from '@nestjs/common';

describe('Frappe Controller', () => {
  let controller: FrappeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [FrappeController],
      providers: [
        { provide: FrappeTokenManagerService, useValue: {} },
        { provide: TokenCacheService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
        { provide: HttpService, useFactory: (...args) => jest.fn() },
      ],
    }).compile();

    controller = module.get<FrappeController>(FrappeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
