import { Entity, ObjectIdColumn, ObjectID, Column, BaseEntity } from 'typeorm';

@Entity()
export class FrappeToken extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  providerUuid: string;

  @Column()
  userUuid: string;

  @Column()
  accessToken: string;

  @Column()
  refreshToken: string;

  @Column()
  idToken: string;

  @Column()
  expirationTime: Date;
}
