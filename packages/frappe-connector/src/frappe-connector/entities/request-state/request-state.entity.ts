import {
  ObjectID,
  ObjectIdColumn,
  Entity,
  Column,
  Index,
  BaseEntity,
} from 'typeorm';

@Entity()
export class RequestState extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  redirect: string;

  @Column()
  userUuid: string;

  @Column()
  providerUuid: string;

  @Column()
  @Index({ expireAfterSeconds: 120 })
  creation: Date;
}
