export const ADMINISTRATOR = 'administrator';
export const TOKEN = 'token';
export const AUTHORIZATION = 'authorization';
export const SERVICE = 'frappe-connector';
export const PUBLIC = 'public';
export const APP_NAME = 'frappe-connector';
export const SWAGGER_ROUTE = 'api-docs';
export const COMMUNICATION_SERVER = 'communication-server';
